class Product {
	constructor(title,imageUrl,description,price){
		this.title = title
		this.imageUrl = imageUrl
		this.description = description
		this.price = price
	}

}

class Cart {
	items = []

	render (){
		const cartEI = document.createElement('section')
		cartEI.innerHTML = `
			<h2>Total : ${0}</h2>
			<button>Order Now </button>
		`
		cartEI.className='cart'
		return cartEI
	}
}


class ProductItem {
	constructor(product){
		this.product = product
	}

	addToCart(){
		console.log("Nambah ke keranjang produk ini :")
		console.log(this.product)
	}

	process(){
		const proudEI = document.createElement('li')
		proudEI.className = 'product-item'
		proudEI.innerHTML = `
			<div>
				<img src="${this.product.imageUrl}" alt="ini gambar products">
				<div class="product_item_content">
					<h2>${this.product.title}</h2>
					<p>${this.product.description}</p>
					<h3>Rp.${this.product.price},-</h3>
					<button> Add to cart </button>
				</div>
			</div>
		`

		const addCardButton = proudEI.querySelector('button')
		addCardButton.addEventListener('click',this.addToCart.bind(this))
		return proudEI
	}
}


class ProductList {
	products = [
		new Product('Books',
			'https://asysyariah.com/wp-content/uploads/2016/07/tumpukan-buku.jpg',
			'Best seller',
			75000
		),
		new Product('Pillows',
			'https://ds393qgzrxwzn.cloudfront.net/resize/m600x500/cat1/img/images/0/WWyOuleQBU.jpg',
			'Nyaman digunakan',
			25000
		),
	]

	constructor() {}

	render(){
		// dibawah ini intinya bikin html code yang di append(masukan) ke dalam html 
		const prodList = document.createElement('ul')
		prodList.className = 'product-list'
		for (const proud of this.products) {
			const productItem = new ProductItem(proud)
			const proudEI = productItem.process()
			prodList.append(proudEI)
		}
		return prodList
	}
}

class Shop {
	render() {
		const renderHook = document.getElementById('app')
		const cart = new Cart()
		const cartEI = cart.render()
		const productList =  new ProductList()
		const prodListEI = productList.render()

		renderHook.append(cartEI)
		renderHook.append(prodListEI)
	}
}
const shop = new Shop()
shop.render()